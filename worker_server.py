
from concurrent import futures
from PIL import Image
from threading import Thread, Lock
from queue import Queue

import sys
import grpc
import inferSys_pb2
import inferSys_pb2_grpc

import time 
import logging
import nn

_ONE_DAY_IN_SECONDS = 60 * 60 * 24

request_queue = Queue(10000)
manager_addr = "shark1:50051"
MAX_BATCH_SIZE = 32

class Batch(object):
    def __init__(self):
        self.key_values = {}
        self.lock = Lock()
        self.lock.acquire()

    def add(self, name):
        self.key_values[name] = None
    
    def done(self):
        self.lock.release()

    def get_result(self):
        with self.lock:
            return self.key_values
    
    def __len__(self):
        return len(self.key_values)


class WorkerServer(inferSys_pb2_grpc.workerServerServicer):
    def __init__(self, addr):
        self.name = addr
        self.batch_size = MAX_BATCH_SIZE
    
    def heart_beat(self, request, context):
        return inferSys_pb2.HeartBeat(name=self.name, isAlive=True, isIdle=True)

    def inference(self, imagefile_iterator, context):
        batch = Batch()
        for image in imagefile_iterator:
            batch.add(image.name)

        request_queue.put(batch) 
        print("queue put")
        names_results = batch.get_result() # dict()
        print(batch.key_values)
        for name_ in names_results:
            yield inferSys_pb2.InferenceResult(name=name_, content=names_results[name_])
        #result = self.inceptionV3.predict(batch.names)


class WorkerClient(object):
    def __init__(self):
        self.model = nn.Inceptionv3()

    def compute(self):
        print("batch.qsize():", request_queue.qsize())
        batch = request_queue.get()
        if len(batch) == 0:
            return
        results = self.model.predict(batch.key_values)
        for name, result in zip(batch.key_values, results):
            batch.key_values[name] = result 
        batch.done()

 
def serve(argv):

    # server
    argv.append("shark1:50055")
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    inferSys_pb2_grpc.add_workerServerServicer_to_server(WorkerServer(argv[1]), server)
    server.add_insecure_port(argv[1])
    server.start()

    # client
    client = WorkerClient()
    
    print("WorkerServer start.")

    try:
        while True:
            client.compute()
            
            #time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)

if __name__ == "__main__":
    serve(sys.argv)



