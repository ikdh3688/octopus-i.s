

import tensorflow as tf
from abc import abstractmethod

IMAGENET_NUM_TRAIN_IMAGES = 1281167
IMAGENET_NUM_VAL_IMAGES = 50000
data_dir = "/home/ssl/inception_cifar_data/convert_ilsvrc2012/"


class DataSet(object):
    def __init__(self, format_name, batchSize):
        self.format_name = format_name
        self.data_dir = data_dir
        self.training_filenames = None
        self.validation_filenames = None
        self.iterator = None
        self.batchSize = batchSize or 32
    
    @abstractmethod
    def _parse_function(self):
        pass

    @abstractmethod
    def _make_iterator(self):
        pass

    @abstractmethod
    def nextBatch(self, batchSize):
        pass
    
    def __preprocessor__(self):
        pass

class tfRecord(DataSet):
    def __init__(self, format_name, batchSize):
        super(tfRecord, self).__init__(format_name, batchSize)

        self.training_filenames = [self.data_dir + "train-00000-of-00050.tfrecord"]
        self.sess = tf.Session()
        self.iterator = self._make_iterator()

    def _parse_function(self, example_proto):
        # Dense features in Example proto.
        feature_map = {
            'image/encoded': tf.FixedLenFeature([], dtype=tf.string,
                                                    default_value=''),
            'image/class/label': tf.FixedLenFeature([1], dtype=tf.int64,
                                                    default_value=-1),
            'image/class/text': tf.FixedLenFeature([], dtype=tf.string,
                                                    default_value=''),
        }

        parsed_features = tf.parse_single_example(example_proto, feature_map)

        return parsed_features["image/encoded"],  \
                parsed_features["image/class/label"], \
                parsed_features["image/class/text"]

    def _make_iterator(self):
        filenames = tf.placeholder(tf.string, shape=[None])

        dataset = tf.data.TFRecordDataset(filenames)
        dataset = dataset.map(self._parse_function)
        dataset = dataset.batch(self.batchSize)

        iterator = dataset.make_initializable_iterator()
        training_filenames = self.training_filenames

        self.sess.run(iterator.initializer, feed_dict={filenames: training_filenames})

        return iterator

    def nextBatch(self):
        return self.sess.run(self.iterator.get_next())

    def __preprocessor__(self):
        pass

    def print_dataset_info(self):
        print("output_classes", dataset.output_classes)
        print("output_types", dataset.output_types)
        print("output_shapes", dataset.output_shapes)


data = tfRecord("tfRecord", 2)
print(data.nextBatch()[1])
print(data.nextBatch()[2])


