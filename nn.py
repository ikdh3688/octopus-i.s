from util import label
from keras.applications.inception_v3 import InceptionV3
from keras.applications.inception_v3 import preprocess_input, decode_predictions
from keras.preprocessing import image
from keras import backend as K
from PIL import Image


import sys

import tensorflow as tf
import numpy as np


class Inceptionv3():
    def __init__(self):
        self.model = self.get_model()

    def get_model(self):
        model = InceptionV3(include_top=True, weights='imagenet', input_tensor=None, input_shape=None)
        return model

    def predict(self, file_names):
        batch = self.preprocess_batch(file_names)
        result = self.model.predict(batch)
        label_ = []
        for i in result:
            label_.append(label[np.argmax(i)])
        return label_
    
    def preprocess_batch(self, file_names):
        img_list = []
        for file_name in file_names:
            PIL_image = Image.open("./data/"+file_name)
            x = image.img_to_array(PIL_image)
            x = np.expand_dims(x, axis=0)
            x = preprocess_input(x)
            img_list.append(x)
        return np.vstack(img_list)
       

# This main function is only used for testing.
if __name__ == "__main__":
    model = Inceptionv3()
    from PIL import Image
    img = Image.open('1.jpg')

    mode = img.mode
    size = img.size
    data = img.tobytes()

    new_img = Image.frombytes(mode, size, data)

    batch = np.vstack([x,y])
    
    print(model.predict(batch))
    print(type(model.predict(batch)))

