
from concurrent import futures
from threading import Thread, Lock 
from queue import Queue
import threading

import sys
import grpc
import inferSys_pb2
import inferSys_pb2_grpc

import time 
import random
import logging

_ONE_DAY_IN_SECONDS = 60 * 60 * 24

request_queue = Queue(10000)
manager_addr = "shark1:50051"
MAX_BATCH_SIZE = 32

class Image(object):
    def __init__(self, name):
        self.name = name
        self.lock = Lock()
        self.lock.acquire()
        self.result = None
    
    def done(self):
        self.lock.release()

    def get_result(self):
        #assert self.result is not None
        with self.lock:
            return self.result

class ProxyServer(inferSys_pb2_grpc.proxyServerServicer):
    def __init__(self, addr):
        self.name = addr
        self.max_batch_size = MAX_BATCH_SIZE

    def heart_beat(self, request, context):
        return inferSys_pb2.HeartBeat(name=self.name, isAlive=True, isIdle=True)

    def put_image(self, imagefile, context):
        global request_queue
        image = Image(imagefile.name)
        image.lock.acquire()
        request_queue.put(image)
        result = image.result()
        
        return inferSystem_pb2.InferenceResult(name=result.name, content=result.result)
        #return inferSystem_pb2.InferenceResult(content=image.result)

class ProxyClient(object):
    def __init__(self):
        self.manager_stub = None
        self.cluster_spec = None
        self.worker_stubs = None
        self.max_batch_size = MAX_BATCH_SIZE
        self.batch = {}

    def connect(self):
        print("ProxyServer Try Connection...")
        self.manager_stub = self.get_manager_stub()
        self.cluster_spec = self.manager_stub.get_cluster_spec(inferSys_pb2.Empty())
        self.worker_stubs = self.get_worker_stub()
        print("ProxyServer Connect:Done.")
    
    def get_worker_stub(self):
        ret = []
        for worker_addr in self.cluster_spec.worker_servers:
            gRPC_channel = grpc.insecure_channel(worker_addr)
            grpc.channel_ready_future(gRPC_channel).result()
            ret.append(inferSys_pb2_grpc.workerServerStub(gRPC_channel))
        return ret

    def get_manager_stub(self):
        gRPC_channel = grpc.insecure_channel(manager_addr)
        grpc.channel_ready_future(gRPC_channel).result()
        manager_stub = inferSys_pb2_grpc.managerStub(gRPC_channel)
        return manager_stub

    def generate_batch(self, batch_size):
        for i in range(batch_size):
            image = request_queue.get()
            self.batch[image.name] = image
            yield inferSys_pb2.ImageFileName(name=image.name)
    
    def job_dispatch(self):
        global request_queue
        worker_stub = random.choice(self.worker_stubs)
        qsize = request_queue.qsize()
        if qsize == 0:
            return
        batch_size = min(qsize, self.max_batch_size)
        responses = worker_stub.inference(self.generate_batch(batch_size))
        for response in responses:
            image = self.batch[response.name]
            #print("job_dispatch",image.lock)
            image.result = response.content
            image.done()
            #del self.batch[response.name]
    
class Producer(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self) 

    def run(self):
        global request_queue
        images = []
        for i in range(1000):
            image = Image(str(i+1) + ".jpg")
            images.append(image)
            request_queue.put(image)

        for image in images:
            res = image.get_result()
            if res is not None:
                print(image.name, "result:", res)


 
def serve(argv):

    # server
    argv.append("shark1:50052") # Test
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    inferSys_pb2_grpc.add_proxyServerServicer_to_server(ProxyServer(argv[1]), server)
    server.add_insecure_port(argv[1])
    server.start()
 
    # client
    client = ProxyClient()
    client.connect()
   
    print("ProxyServer start.")

    # <Test> Producer
    producer = Producer()
    producer.start()
    pool = futures.ThreadPoolExecutor(max_workers=10)

    try:
        while True:
            #time.sleep(1)
            pool.submit(client.job_dispatch)
            #time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)

if __name__ == "__main__":
    serve(sys.argv)



