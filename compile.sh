#!/bin/bash
source ./env.sh

python -m grpc_tools.protoc -I${HOME_DIR}/protos --python_out=. --grpc_python_out=. ${HOME_DIR}/protos/inferSys.proto
