
import grpc
import inferSys_pb2
import inferSys_pb2_grpc 

import sys
import numpy as np
import random 

manager_addr = "shark1:50051"

def get_cluster_spec():
    gRPC_channel = grpc.insecure_channel(manager_addr)
    manager_stub = inferSys_pb2_grpc.managerStub(gRPC_channel)
    return manager_stub.get_cluster_spec(inferSys_pb2.Empty())


def main(argv):
    # Image file path.
    file_path = argv[1] 

    # Get the cluster spec from the manager.
    cluster_spec = get_cluster_spec()
    proxy_server_addr = random.choice(cluster_spec.proxy_servers)

    # Creating gRPC message.
    message = inferSys_pb2.ImageFileName(name=file_path)
    
    # Preparation of gRPC stub to a proxy server.
    gRPC_channel = grpc.insecure_channel(proxy_server_addr)
    proxyServer_stub = inferSys_pb2_grpc.proxyServerStub(gRPC_channel)

    # Call RPC service.
    result = proxyServer_stub.put_image(message)
    print(result.content)


if __name__ == "__main__":
    main(sys.argv)


