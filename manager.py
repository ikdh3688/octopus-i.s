
import grpc
import inferSys_pb2
import inferSys_pb2_grpc 

import sys
import time
from concurrent import futures

_ONE_DAY_IN_SECONDS = 60 * 60 * 24
manager_addr = "shark1:50051"
Cluster_Spec = None

class ManagerGrpcServer(inferSys_pb2_grpc.managerServicer):
    def __init__(self):
        pass

    def get_cluster_spec(self, request, context):
        global Cluster_Spec
        return Cluster_Spec

class ManagerGrpcClient(object):
    def __init__(self, cluster_spec):
        self.cluster_spec = cluster_spec
        self.channels = None
    
    def connect(self):
        channels = {}
        channels['ps'] = {}
        channels['w'] = {}

        for ps in self.cluster_spec.proxy_servers:
            gRPC_channel = grpc.insecure_channel(ps)
            grpc.channel_ready_future(gRPC_channel).result()
            stub = inferSys_pb2_grpc.proxyServerStub(gRPC_channel)
            channels['ps'][gRPC_channel] = stub

        for w in self.cluster_spec.worker_servers:
            gRPC_channel = grpc.insecure_channel(w)
            grpc.channel_ready_future(gRPC_channel).result()
            stub = inferSys_pb2_grpc.workerServerStub(gRPC_channel)
            channels['w'][gRPC_channel] = stub
        self.channels = channels

        print("Manager Connect:Done.")

    def check_alive(self):
        for server_type in self.channels:
            for channel in self.channels[server_type]:
                stub = self.channels[server_type][channel]
                try:
                    heartbeat = stub.heart_beat(inferSys_pb2.Empty())
                    print("%s is alive" % (server_type))
                except:
                    print("%s is not alive." % (server_type))
                    future = grpc.channel_ready_future(channel)
                    


def make_config():
    cluster_spec = inferSys_pb2.ClusterSpec()
    with open('config', 'rb') as f:
        cluster_spec.ParseFromString(f.read())
    return cluster_spec
    

def main(argv):
    global Cluster_Spec
    Cluster_Spec = make_config()

    thread_pool = futures.ThreadPoolExecutor(max_workers=10)

    # manager server
    server = grpc.server(thread_pool)
    inferSys_pb2_grpc.add_managerServicer_to_server(ManagerGrpcServer(), server)
    server.add_insecure_port(manager_addr)
    server.start()

    # manager client
    client = ManagerGrpcClient(Cluster_Spec)
    client.connect()
    time.sleep(1)
    print("Manager start.")

    try:
        while True:
            client.check_alive()
            time.sleep(3)
            #time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)

if __name__ == "__main__":
    main(sys.argv)


